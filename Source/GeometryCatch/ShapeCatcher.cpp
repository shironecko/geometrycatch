// Fill out your copyright notice in the Description page of Project Settings.

#include "ShapeCatcher.h"


// Sets default values
AShapeCatcher::AShapeCatcher(const FObjectInitializer &OI)
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

    SceneComponent = OI.CreateDefaultSubobject<USceneComponent>(this, TEXT("Root"));
    RootComponent = SceneComponent;

    SpriteComponent = OI.CreateDefaultSubobject<UPaperSpriteComponent>(this, TEXT("Sprite"));
    SpriteComponent->SetupAttachment(RootComponent);
    SpriteComponent->SetRelativeTransform(FTransform(FRotator(-90.0f, 0, 0), FVector::ZeroVector, FVector(0.25f)));
    SpriteComponent->SetCollisionProfileName(FName(TEXT("Pawn")));
    SpriteComponent->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
}

// Called when the game starts or when spawned
void AShapeCatcher::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AShapeCatcher::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

    AddActorWorldOffset(GetTransform().GetRotation().GetForwardVector() * MovementInput * MovementSpeed * DeltaTime);
}

// Called to bind functionality to input
void AShapeCatcher::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

    PlayerInputComponent->BindAxis(TEXT("ShapeCatcherMove"), this, &AShapeCatcher::OnMovementInput);
}

void AShapeCatcher::NotifyActorBeginOverlap(AActor* OtherActor)
{
    Super::NotifyActorBeginOverlap(OtherActor);

    if (OtherActor && OtherActor->IsA<AFallingShape>())
    {
        AFallingShape *Shape = Cast<AFallingShape>(OtherActor);
        OnShapeCatchedEvent.Broadcast(0, Shape);
        SpriteComponent->SetSpriteColor(Shape->ShapeColor);

        Shape->Destroy();
    }
}

void AShapeCatcher::OnMovementInput(float Input)
{
    MovementInput = FMath::Clamp(Input, -1.0f, 1.0f);
}

