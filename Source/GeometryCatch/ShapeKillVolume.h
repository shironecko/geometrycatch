// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/TriggerVolume.h"
#include "ShapeKillVolume.generated.h"

/**
 * 
 */
UCLASS()
class GEOMETRYCATCH_API AShapeKillVolume : public ATriggerVolume
{
	GENERATED_BODY()
	
public:
	void NotifyActorBeginOverlap(AActor* OtherActor) override;
	
};
