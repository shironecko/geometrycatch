// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "PaperSpriteComponent.h"
#include "FallingShape.generated.h"

UENUM(BlueprintType)
enum class EShapeType : uint8
{
    Circle,
    Triangle,
    Pentagon,
    Star,

    Count
};

USTRUCT(BlueprintType)
struct FShapeParameters
{
    GENERATED_BODY()

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    UPaperSprite *Sprite = nullptr;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    FColor Color = FColor(255, 255, 255);
};

UCLASS(Transient)
class GEOMETRYCATCH_API AFallingShape : public AActor
{
	GENERATED_BODY()
	
public:
    AFallingShape(const FObjectInitializer& ObjectInitializer);

protected:
	virtual void BeginPlay() override;

public:
    virtual void OnConstruction(const FTransform& Transform) override;

	virtual void Tick(float DeltaTime) override;

    void SetShapeType(EShapeType Type);

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FVector FallingDirection = FVector(0, -1.0f, 0);

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    float FallingSpeed = 30.0f;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    EShapeType ShapeType = EShapeType::Circle;

    FColor ShapeColor;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, EditFixedSize)
    TArray<FShapeParameters> ShapeParameters;

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
    USceneComponent *SceneComponent;

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
    UPaperSpriteComponent *SpriteComponent;
};
