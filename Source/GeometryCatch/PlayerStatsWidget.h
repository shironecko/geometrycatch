// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "PlayerStatsWidget.generated.h"

/**
 * 
 */
UCLASS()
class GEOMETRYCATCH_API UPlayerStatsWidget : public UUserWidget
{
	GENERATED_BODY()
	
public:
    UFUNCTION(BlueprintImplementableEvent, Category = "Player Stats")
    void OnPlayerStatsUpdate(float ComboCounter, float PointsAccumulated);
	
	
};
