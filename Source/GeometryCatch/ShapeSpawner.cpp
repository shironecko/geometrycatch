// Fill out your copyright notice in the Description page of Project Settings.

#include "ShapeSpawner.h"


// Sets default values
AShapeSpawner::AShapeSpawner(const FObjectInitializer &OI)
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

    RootComponent = OI.CreateDefaultSubobject<USceneComponent>(this, TEXT("Root"));
}

// Called when the game starts or when spawned
void AShapeSpawner::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AShapeSpawner::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

    TimeTillNextShape -= DeltaTime;

    if (TimeTillNextShape <= 0)
    {
        TimeTillNextShape += FMath::RandRange(ShapeSpawnDelayMin, ShapeSpawnDelayMax);
        if (ShapeClass)
        {
            FVector Pos = GetTransform().GetLocation();
            Pos.X += FMath::RandRange(-ShapeSpawnRangeY, ShapeSpawnRangeY);
            AFallingShape *Shape = GetWorld()->SpawnActor<AFallingShape>(ShapeClass, Pos, FRotator::ZeroRotator);
            Shape->SetShapeType((EShapeType)(FMath::Rand() % (int)EShapeType::Count));
            Shape->FallingSpeed = FMath::RandRange(ShapeSpeedMin, ShapeSpeedMax);
        }
    }
}

