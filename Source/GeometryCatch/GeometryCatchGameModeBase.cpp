// Fill out your copyright notice in the Description page of Project Settings.

#include "GeometryCatchGameModeBase.h"
#include "ShapeCatcher.h"



AGeometryCatchGameModeBase::AGeometryCatchGameModeBase()
{
    PrimaryActorTick.bCanEverTick = true;
}

void AGeometryCatchGameModeBase::BeginPlay()
{
    Super::BeginPlay();

    if (HUDWidgetClass)
    {
        HUDWidget = CreateWidget<UGameHUD>(GetGameInstance(), HUDWidgetClass);
        if (!HUDWidget)
        {
            UE_LOG(LogTemp, Error, TEXT("Failed to create HUD widget!"));
        }
        else
        {
            HUDWidget->AddToViewport();
        }
    }

    APlayerController *FirstPlayerController = GetWorld()->GetFirstPlayerController();
    AShapeCatcher *FirstPlayerPawn = FirstPlayerController ? Cast<AShapeCatcher>(FirstPlayerController->GetPawn()) : nullptr;
    if (FirstPlayerPawn)
    {
        FirstPlayerPawn->OnShapeCatchedEvent.AddUObject(this, &AGeometryCatchGameModeBase::OnShapeCatchedByPlayer);
    }
}

void AGeometryCatchGameModeBase::Tick(float DeltaTime)
{
    GameplayStats.CountdownTime -= DeltaTime;
    GameplayStats.CountdownTime = FMath::Max(GameplayStats.CountdownTime, 0.0f);

    if (HUDWidget)
    {
        HUDWidget->OnUpdateCountdownTimer(GameplayStats.CountdownTime);
    }
}

void AGeometryCatchGameModeBase::OnShapeCatchedByPlayer(int32 PlayerIndex, AFallingShape *Shape)
{
    FPlayerStats &PlayerStats = PlayerIndex == 0 ? GameplayStats.PlayerOneStats : GameplayStats.PlayerTwoStats;

    if (PlayerStats.LastCatchedShape == Shape->ShapeType)
    {
        PlayerStats.ComboCounter += 1;
    }
    else
    {
        PlayerStats.ComboCounter = 0;
    }
    PlayerStats.LastCatchedShape = Shape->ShapeType;
    PlayerStats.PointsAccumulated += PlayerStats.ComboCounter;
    if (HUDWidget)
    {
        if (PlayerIndex == 0)
        {
            HUDWidget->UpdatePlayerOneStats(GameplayStats.PlayerOneStats.ComboCounter, GameplayStats.PlayerOneStats.PointsAccumulated);
        }
        else if (PlayerIndex == 1)
        {
        }
    }
}
