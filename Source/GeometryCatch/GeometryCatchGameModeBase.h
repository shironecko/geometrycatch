// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "FallingShape.h"
#include "ShapeCatcher.h"
#include "GameHUD.h"
#include "Blueprint/UserWidget.h"
#include "GeometryCatchGameModeBase.generated.h"

USTRUCT(BlueprintType)
struct FPlayerStats
{
    GENERATED_BODY()

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    EShapeType LastCatchedShape = EShapeType::Count;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    float ComboCounter = 0;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    float PointsAccumulated = 0;
};

USTRUCT(BlueprintType)
struct FGameplayStats
{
    GENERATED_BODY()

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    FPlayerStats PlayerOneStats;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    FPlayerStats PlayerTwoStats;

    float CountdownTime = 10.0f;
};

UCLASS()
class GEOMETRYCATCH_API AGeometryCatchGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
public:
    AGeometryCatchGameModeBase();

	virtual void BeginPlay() override;

    virtual void Tick(float DeltaTime) override;

    void OnShapeCatchedByPlayer(int32 PlayerIndex, AFallingShape *Shape);
	
    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    FGameplayStats GameplayStats;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    TSubclassOf<UGameHUD> HUDWidgetClass;

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
    UGameHUD *HUDWidget;
};
