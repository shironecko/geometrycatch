// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "FallingShape.h"
#include "ShapeSpawner.generated.h"

UCLASS()
class GEOMETRYCATCH_API AShapeSpawner : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AShapeSpawner(const FObjectInitializer &OI);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

    UPROPERTY(EditAnywhere)
	TSubclassOf<AFallingShape> ShapeClass;

    UPROPERTY(EditAnywhere)
    float ShapeSpawnRangeY = 500.0f;

    UPROPERTY(EditAnywhere)
    float ShapeSpawnDelayMin = 0.05f;

    UPROPERTY(EditAnywhere)
    float ShapeSpawnDelayMax = 0.3f;

    UPROPERTY(EditAnywhere)
    float ShapeSpeedMin = 200.0f;

    UPROPERTY(EditAnywhere)
    float ShapeSpeedMax = 600.0f;

private:
    float TimeTillNextShape = ShapeSpawnDelayMin;
	
};
