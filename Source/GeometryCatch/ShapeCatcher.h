// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "PaperSpriteComponent.h"
#include "FallingShape.h"
#include "ShapeCatcher.generated.h"

DECLARE_EVENT_TwoParams(AShapeCatcher, OnShapeCatched, int32, AFallingShape*);

UCLASS()
class GEOMETRYCATCH_API AShapeCatcher : public APawn
{
	GENERATED_BODY()

public:
	AShapeCatcher(const FObjectInitializer &OI);

protected:
	virtual void BeginPlay() override;

public:	

    OnShapeCatched OnShapeCatchedEvent;

	virtual void Tick(float DeltaTime) override;

	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

    virtual void NotifyActorBeginOverlap(AActor* OtherActor) override;

    void OnMovementInput(float Input);

    UPROPERTY(VisibleAnywhere)
	USceneComponent *SceneComponent;

    UPROPERTY(VisibleAnywhere)
    UPaperSpriteComponent *SpriteComponent;

    UPROPERTY(EditAnywhere)
    float MovementSpeed = 100.0f;

private:
    float MovementInput = 0;
	
};
