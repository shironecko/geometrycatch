// Fill out your copyright notice in the Description page of Project Settings.

#include "FallingShape.h"

AFallingShape::AFallingShape(const FObjectInitializer& ObjectInitializer)
{
    PrimaryActorTick.bCanEverTick = true;

    SceneComponent = ObjectInitializer.CreateDefaultSubobject<USceneComponent>(this, TEXT("Root"));
    RootComponent = SceneComponent;

    SpriteComponent = ObjectInitializer.CreateDefaultSubobject<UPaperSpriteComponent>(this, TEXT("Sprite Render"));
    SpriteComponent->SetupAttachment(RootComponent);
    SpriteComponent->SetRelativeTransform(FTransform(FRotator(-90.0f, 0, 0), FVector(0), FVector(0.25f)));

    ShapeParameters.SetNum((int32)EShapeType::Count);
}

void AFallingShape::BeginPlay()
{
	Super::BeginPlay();
	
    SetShapeType(ShapeType);
}

void AFallingShape::OnConstruction(const FTransform& Transform)
{
    SetShapeType(ShapeType);
}

void AFallingShape::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

    FVector MoveVector = DeltaTime * FallingSpeed * FallingDirection;
    this->AddActorWorldOffset(MoveVector);
}

void AFallingShape::SetShapeType(EShapeType Type)
{
    check(ShapeType < EShapeType::Count);
    ShapeType = Type;
    FShapeParameters& Params = ShapeParameters[(int32)ShapeType];
    SpriteComponent->SetSprite(Params.Sprite);
    SpriteComponent->SetSpriteColor(Params.Color);
    ShapeColor = Params.Color;
}

