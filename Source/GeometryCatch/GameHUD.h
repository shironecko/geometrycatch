// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Blueprint/WidgetTree.h"
#include "PlayerStatsWidget.h"
#include "GameHUD.generated.h"

/**
 * 
 */
UCLASS()
class GEOMETRYCATCH_API UGameHUD : public UUserWidget
{
	GENERATED_BODY()
	
public:
    virtual void NativeConstruct() override;

    UFUNCTION(BlueprintImplementableEvent, Category = "GameHUD")
    void OnUpdateCountdownTimer(float CountdownTime);

    UFUNCTION(BlueprintCallable, Category = "GameHUD")
    void SetPlayerStatsWidgets(UPlayerStatsWidget *PlayerOneStats, UPlayerStatsWidget *PlayerTwoStats);

    void UpdatePlayerOneStats(float ComboCount, float Points);
	
    UPROPERTY(EditAnywhere)
	TSubclassOf<UPlayerStatsWidget> PlayerStatsWidgetClass;

    UPROPERTY(EditAnywhere, BlueprintReadOnly)
    UPlayerStatsWidget *PlayerOneStatsWidget;

    UPROPERTY(EditAnywhere, BlueprintReadOnly)
    UPlayerStatsWidget *PlayerTwoStatsWidget;
};
