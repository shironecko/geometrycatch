// Fill out your copyright notice in the Description page of Project Settings.

#include "GameHUD.h"

void UGameHUD::NativeConstruct()
{
    Super::NativeConstruct();

    if (WidgetTree)
    {
        PlayerOneStatsWidget = Cast<UPlayerStatsWidget>(WidgetTree->FindWidget("PlayerOneStats"));
        PlayerTwoStatsWidget = Cast<UPlayerStatsWidget>(WidgetTree->FindWidget("PlayerTwoStats"));
    }
}

void UGameHUD::SetPlayerStatsWidgets(UPlayerStatsWidget *PlayerOneStats, UPlayerStatsWidget *PlayerTwoStats)
{
    /*PlayerOneStatsWidget = PlayerOneStats;
    PlayerTwoStatsWidget = PlayerTwoStats;*/
}

void UGameHUD::UpdatePlayerOneStats(float ComboCount, float Points)
{
    if (PlayerOneStatsWidget)
    {
        PlayerOneStatsWidget->OnPlayerStatsUpdate(ComboCount, Points);
    }
}
