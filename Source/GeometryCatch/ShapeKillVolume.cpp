// Fill out your copyright notice in the Description page of Project Settings.

#include "ShapeKillVolume.h"
#include "FallingShape.h"




void AShapeKillVolume::NotifyActorBeginOverlap(AActor* OtherActor)
{
    if (OtherActor && OtherActor->IsA<AFallingShape>())
    {
        OtherActor->Destroy();
    }
}
